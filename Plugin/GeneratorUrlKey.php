<?php

namespace Mbs\ProductUrlKey\Plugin;

class GeneratorUrlKey
{
    public function aroundGetUrlKey(
        \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $subject,
        callable $proceed,
        \Magento\Catalog\Model\Product $product
    ) {
        $brand = $this->getBrandProductName($product);

        if ($brand!= '') {
            $originalProductName = $product->getName();

            $product->setName($brand . ' '. $originalProductName);
            $result = $proceed($product);
            $product->setName($originalProductName);
        } else {
            $result = $proceed($product);
        }

        return $result;
    }

    private function getBrandProductName(\Magento\Catalog\Model\Product $product)
    {
        return $product->getAttributeText('brand');
    }
}
