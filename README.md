# README #

Change product url key prepending it with the brand product value

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/ProductUrlKey when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

Create a new product in the backend and verify if the product has a brand that the brand value is prepended to the url key